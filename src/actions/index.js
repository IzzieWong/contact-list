import "whatwg-fetch";

export const SELECT_USER = "SELECT_USER";
export const REQUEST_USERS = "REQUEST_USERS";
export const RECEIVE_USERS = "RECEIVE_USERS";
export const SEARCH_USERS_BY_NAME = "SEARCH_USERS_BY_NAME";
export const SET_VISIBILITY_FILTER = "SET_VISIBILITY_FILTER";
export const INVALIDATE_USERS = "INVALIDATE_USERS";
export const RESET_ERROR_MESSAGE = "RESET_ERROR_MESSAGE";

const searchUsers = (users) => ({
  type: SEARCH_USERS_BY_NAME,
  users
});

const selectUser = (user) => ({
  type: SELECT_USER,
  user
});

const invalidateUser = () => ({
  type: INVALIDATE_USERS
});

const requestUsers = () => ({
  type: REQUEST_USERS
});

const receiveUsers = (json) => ({
  type: RECEIVE_USERS,
  users: json
});

const fetchUsers = (uid) => (dispatch) => {
  dispatch(requestUsers());
  return fetch("http://jsonplaceholder.typicode.com/users")
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      return dispatch(invalidateUser());
    })
    .then(json => {
      dispatch(receiveUsers(json));
      if (uid) {
        const user = json.filter(item => item.id === parseInt(uid, 10));
        dispatch(selectUser(user[0]));
      }
    });
};

const shouldFetchUsers = (state) => {
  const users = state.users;
  if (!users || !users.items || users.items.length === 0) {
    return true;
  }
  if (users.isFetching) {
    return false;
  }
  return users.didInvalidate;
};

export const fetchUsersIfNeeded = () => (dispatch, getState) => {
  if (shouldFetchUsers(getState())) {
    return dispatch(fetchUsers());
  }
  return false;
};

export const fetchUserById = (uid) => (dispatch, getState) => {
  if (shouldFetchUsers(getState())) {
    return dispatch(fetchUsers(uid));
  }
  const { users } = getState();
  const user = users.items.filter(item => item.id === parseInt(uid, 10));
  return dispatch(selectUser(user[0]));
};

export const setVisibilityFilter = (filter) => ({
  type: SET_VISIBILITY_FILTER,
  filter
});

export const searchUsersByName = (name) => (dispatch, getState) => {
  const { users } = getState();
  const nextUsers = users.items.filter(item => item.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  return dispatch(searchUsers(nextUsers));
};

export const resetErrorMessage = () => ({
  type: RESET_ERROR_MESSAGE
});

