import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { fetchUsersIfNeeded, searchUsersByName, setVisibilityFilter } from "../actions";
import List from "../components/List";
import Search from "../components/Search";
import Filter from "../components/Filter";
import orderBy from "lodash/orderBy";

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchUsersIfNeeded());
  }

  handleSearch(nextValue) {
    const { dispatch } = this.props;
    dispatch(searchUsersByName(nextValue));
  }

  handleFilter(sort) {
    const { dispatch } = this.props;
    const nextFilter = sort === "asc" ? "SHOW_ALPHABET_ASC" : "SHOW_ALPHABET_DESC";
    dispatch(setVisibilityFilter(nextFilter));
  }

  render() {
    const { users, filteredUsers } = this.props;
    const isFetching = users.isFetching;
    const isEmpty = filteredUsers.length === 0;
    let template;
    if (!isEmpty) {
      template = (<List items={filteredUsers}/>);
    } else {
      template = (isFetching ? <h2>Loading...</h2> : <h2>No results.</h2>);
    }

    return (
      <div>
        <div className="clearfix">
          <Search onChange={this.handleSearch}/>
          <Filter onClick={this.handleFilter}/>
        </div>
        <br/>
        <div>
          { template }
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  users: PropTypes.object.isRequired,
  filteredUsers: PropTypes.array.isRequired,
  visibilityFilter: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired
};

const getVisibleUsers = (users, filter) => {
  switch (filter) {
    case "SHOW_ALL":
    default:
      return users;
    case "SHOW_ALPHABET_DESC":
      return orderBy(users, ["name"], ["desc"]);
    case "SHOW_ALPHABET_ASC":
      return orderBy(users, ["name"], ["asc"]);
  }
};

const mapStateToProps = (state) => {
  const { users, errorMessage, visibilityFilter } = state;
  const filteredUsers = getVisibleUsers(users.currentItems, visibilityFilter);
  return {
    users,
    filteredUsers,
    visibilityFilter,
    errorMessage
  };
};

export default connect(mapStateToProps)(HomePage);
