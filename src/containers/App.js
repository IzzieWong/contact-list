import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import * as ActionCreators from "../actions";
import Header from "../components/Header";
import "bootstrap-sass";

class App extends Component {
  render() {
    const { children } = this.props;
    return (
      <div className="container">
        <Header />
        <hr/>
        <div className="content">
          {children}
        </div>
      </div>
    );
  }
}

App.propTypes = {
  // Injected by React Router
  children: PropTypes.node
};


export default connect()(App);
