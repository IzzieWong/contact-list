import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { fetchUserById } from "../actions";
import Card from "../components/Card";

class UserPage extends Component {
  componentDidMount() {
    const { dispatch, uid } = this.props;
    dispatch(fetchUserById(uid));
  }

  render() {
    const { selectedUser } = this.props;
    let template = (<h2>Loading...</h2>);
    if (selectedUser && selectedUser.name) {
      template = (<Card { ...selectedUser } />);
    }
    return (
      <div>
        <div className="row">
          { template }
        </div>
        <br/><br/>
        <div className="row">
          <p>P.S. Current mock data doesn't has the node of Job Title. It will auto replace default one if there's the value later.</p>
        </div>
      </div>
    );
  }
}

UserPage.propTypes = {
  uid: PropTypes.string.isRequired,
  selectedUser: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { selectedUser } = state;
  const uid = ownProps.location.pathname.replace("/user/", "");
  return {
    uid,
    selectedUser
  };
};

export default connect(mapStateToProps)(UserPage);
