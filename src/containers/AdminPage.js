import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { fetchUsersIfNeeded } from "../actions";
import orderBy from "lodash/orderBy";
import groupBy from "lodash/groupBy";
import map from "lodash/map";

class AdimPage extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchUsersIfNeeded());
  }

  render() {
    const { users, groupedUsers } = this.props;
    let records = [];
    const isEmpty = groupedUsers.length === 0;
    if (!isEmpty) {
      map(groupedUsers, (item, key) => {
        records.push({ alphabet: key, number: item.length });
      });
    }
    return (
      <div>
        <h4 className="text-center text-uppercase">Reports</h4>
        <p>Total: {users.items.length}</p>
        <table className="table table-striped">
          <thead>
          <tr>
            <th>Alphabet</th>
            <th>Number</th>
          </tr>
          </thead>
          <tbody>
          {records.map(item => (<tr key={item.alphabet}>
            <th scope="row">{item.alphabet}</th>
            <td>{item.number}</td>
          </tr>))}
          </tbody>
        </table>
        {isEmpty ? <h2>Loading...</h2> : ""}
      </div>
    );
  }
}

AdimPage.propTypes = {
  users: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
};

const groupByAlphabet = (users) => {
  let nextUsers = [];
  if (users.length > 0) {
    nextUsers = orderBy(users, ["name"], ["asc"]);
    nextUsers = groupBy(nextUsers, (item) => item.name.substr(0, 1));
    return nextUsers;
  }
  return nextUsers;
};

const mapStateToProps = (state) => {
  const { users } = state;
  const groupedUsers = groupByAlphabet(users.items);
  return {
    users,
    groupedUsers
  };
};

export default connect(mapStateToProps)(AdimPage);
