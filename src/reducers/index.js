import { combineReducers } from "redux";
import * as ActionTypes from "../actions";
import { routerReducer as routing } from "react-router-redux";

const selectedUser = (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.SELECT_USER:
      return action.user;
    default:
      return state;
  }
};

const users = (state = {
  isFetching: false,
  didInvalidate: false,
  items: [],
  currentItems: []
}, action) => {
  switch (action.type) {
    case ActionTypes.FAILURE_USERS:
      return Object.assign({}, state, {
        didInvalidate: true
      });
    case ActionTypes.REQUEST_USERS:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      });
    case ActionTypes.RECEIVE_USERS:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.users,
        currentItems: action.users
      });
    case ActionTypes.SEARCH_USERS_BY_NAME:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        currentItems: action.users
      });
    default:
      return state;
  }
};

const visibilityFilter = (state = "SHOW_ALL", action) => {
  switch (action.type) {
    case ActionTypes.SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
};

// Updates error message to notify about the failed fetches.
const errorMessage = (state = null, action) => {
  const { type, error } = action;
  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null;
  } else if (error) {
    return action.error;
  }
  return state;
};

const rootReducer = combineReducers({
  users,
  selectedUser,
  visibilityFilter,
  errorMessage,
  routing
});

export default rootReducer;
