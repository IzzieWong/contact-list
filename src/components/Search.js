import React, { Component, PropTypes } from "react";

class Search extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(keyword) {
    this.props.onChange(keyword);
  }

  render() {
    return (
      <div className="pull-left">
        <input placeholder="Search.."
               onChange={event => this.handleChange(event.target.value)}
        />
      </div>
    );
  }
}

Search.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default Search;
