import React, { Component, PropTypes } from "react";
import { Link, IndexLink } from "react-router";

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="row">
          <div className="col-xs-12 col-sm-4">
            <h1>Contact List</h1>
          </div>
          <div className="col-xs-12 col-sm-8 header__menu">
            <ul className="list-unstyled list-inline">
              <li><IndexLink to="/" className="btn btn-default" activeClassName="active">Show Contacts</IndexLink></li>
              <li><Link to="/admin" className="btn btn-default" activeClassName="active">Admin</Link></li>
            </ul>
          </div>
        </div>
        <div>
          <p>P.S. Move the DevTools with Ctrl+W or hide them with Ctrl+H.</p>
        </div>
      </header>
    );
  }
}

export default Header;
