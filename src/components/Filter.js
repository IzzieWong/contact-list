import React, { Component, PropTypes } from "react";

class Filter extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event, filter) {
    event.preventDefault();
    this.props.onClick(filter);
  }

  render() {
    return (
      <div className="pull-left">
        <span>Sort:</span>{" "}
        <button onClick={event => this.handleClick(event, "asc")}>A-Z</button>{" "}
        <button onClick={event => this.handleClick(event, "desc")}>Z-A</button>
      </div>
    );
  }
}

Filter.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default Filter;
