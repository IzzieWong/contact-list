import React, { Component, PropTypes } from "react";
import { Link } from "react-router";

class List extends Component {
  render() {
    const { items } = this.props;
    return (
      <ul>
        {items.map(item => {
          const path = `/user/${item.id}`;
          return (<li key={item.id}><Link to={path}>{item.name}</Link></li>);
        }
        )}
      </ul>
    );
  }
}

List.propTypes = {
  items: PropTypes.array.isRequired
};

export default List;
