import React, { Component, PropTypes } from "react";
import { Link } from "react-router";

const formatName = (name) => {
  if (!name) {
    return false;
  }
  let arr = name.split(" ");
  console.log(arr);
  return name;
};

class Card extends Component {
  render() {
    const { name, email, phone, website, company, jobTitle } = this.props;
    const nameArr = name.split(" ");
    return (
      <div className="col-xs-12 col-sm-8 col-lg-6 card">
        <div className="row card__content">
          <div className="col-xs-12 col-sm-7 card__intro">
            <h1>
              {
                nameArr.map((i, k) => (<span key={k} data-content={i}>{i}{" "}</span>))
              }
            </h1>
            <h6>{jobTitle || "Job Title"}</h6>
          </div>
          <div className="col-xs-12 col-sm-5 card__detail">
            <h3>{company.name}</h3>
            <div><b>Phone: </b><i>{phone}</i></div>
            <div><b>E-Mail: </b><i>{email}</i></div>
            <div><b>URL: </b><i>{website}</i></div>
          </div>
        </div>
      </div>
    );
  }
}

Card.propTypes = {};

export default Card;
