import React from "react";
import { Route, IndexRoute } from "react-router";
import App from "./containers/App";
import HomePage from "./containers/HomePage";
import UserPage from "./containers/UserPage";
import AdminPage from "./containers/AdminPage";

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="/user/:uid" component={UserPage} />
    <Route path="/admin" component={AdminPage} />
  </Route>
);
