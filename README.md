# README #

this is a contact list built with React and Redux.

### How do I get set up? ###

* Installation
```
npm install
```

* Running on local
```
npm start
```

* Deployment instructions
```
npm run build
```

### Who do I talk to? ###

* Repo owner
